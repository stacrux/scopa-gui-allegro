//dichiaro le funzioni principali
void doublebuffer(); //consigliata dal libro per non far sfarfallare
void printbkgd();    //stampa lo sfondo
void setcardbitmap(char value, char seme); //imposta le carte
void printcardontable(int m);  //stampa le carte sul tavolo  
void printhands(int m);  //stampa la tua mano
void printpchand(int m,int n); //stampa le carte dei pc
void printpchandgaming(int m,int n); //stampa le carte dei pc
void printmouse();  //stampa il puntatore
void printmouse2(); //stampa il puntatore per selezionare le opzioni 
void checkselection(); //controlla le azioni del mouse
void carteintavola(); //mette le carte in tavola a inizio partita
void daicarte(int m); //da le carte ai giocatori
void markselected(); //stampa le carte selezionate
int valorecarta(char carta); //ritorna il valore numerico della carta
void checkaction(); //controlla la tua mossa
void doubleclick();    
void cronaca(char* stringa,int x,char P); //mettere testo (p � la y)
void cronacacol(char* stringa,int x,char P,int col); //mettere testo colorato (p � la y)
void mossapcIA1(int pcn); //pcn � il player che gioca in quel momento
void mossapcIA2(int pcn);
void mossapcIA3(int pcn);
void mossapcIA4(int pcn);
void contatorepc();//valuta la miglior mossa
void contatorepc2();
void contatorepc3();
char* seme(char sm);
//void semebmp(char sm);
void azzeratemp();
void sommaricorsiva();
void printsum(int z); //inutile 
void scarta();
void scarta2();    
void scarta3();   
int controllapartita();
void setmedalbitmap(char kind);
void printmedal(int xmedal,int ymedal);
int valorecartaprimiera(char carta);
int risultati();
void reset();
void printintro();
int intro();
void deepreset();
void checkationintro();
int pausa(); 
void checkmousepause(); 
int informazioni(int y);
void  checkmouseinfo();
void epilogue(int fine);
void timetolose();
void sommaricorsivaperscartarepc();
void contacarte();
void salva();
int carica();
void aggiustaarraypunti();
//dichiaro le funzioni principali
